
<!DOCTYPE html>
<html lang="zxx">

@include('admin.include.head')
<body>
<div class="container-scroller">
@include('admin.include.sidebar')
@include('admin.include.header')
    <div class="main-panel">
@yield('content')

@include('admin.include.footer')
    </div>
    <!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->

@include('admin.include.foot')
@yield('script')
</body>
</html>
