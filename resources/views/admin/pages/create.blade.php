@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title"> {{__('admin.page')}} </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">{{__('admin.pages')}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{__('admin.add')}} {{__('admin.page')}}</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="card col-md-12">
                <div class="card-body">
                    @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        {{Session::get('success')}}
                    </div>
                    <br>
                    @endif
                    <form class="forms-sample" action="{{route('Pages.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputName1">{{__('admin.name_en')}}</label>
                            <input type="text" class="form-control" id="name_en" name="name_en" placeholder="{{__('admin.name_en')}}">
                        </div>
                        @error('name_en')
                        <span style="color: darkred;" >
                                       * <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        <div class="form-group">
                            <label for="exampleInputName1">{{__('admin.name_ar')}}</label>
                            <input type="text" class="form-control" id="name_ar" name="name_ar" placeholder="{{__('admin.name_ar')}}">
                        </div>
                        @error('name_ar')

                        <span style="color: darkred;" >
                                       * <strong>{{ $message }}</strong>
                                    </span>
                        <br>

                        @enderror


                        <label for="exampleInputName1">{{__('admin.photo')}}</label>

                        <div class="container">
                            <div class="wrapper">
                                <div class="image">
                                    <img src="" alt="" id="upload_image"/>
                                </div>
                                <div class="content">
                                    <div class="icon">
                                        <i class="fas fa-cloud-upload-alt"></i>
                                    </div>
                                    <div class="text">
                                        {{__('admin.no_file')}}
                                    </div>
                                </div>
                                <div id="cancel-btn">
                                    <i class="fas fa-times"></i>
                                </div>
                                <div class="file-name">
                                    {{__('admin.file_name_here')}}
                                </div>
                            </div>
                            <button type="button" onclick="defaultBtnActive()" id="custom-btn">{{__('admin.choose_file')}}</button>
                            <input id="default-btn" type="file" hidden="hidden" name="photo">
                        </div>
                        @error('photo')

                        <span style="color: darkred;" >
                                       * <strong>{{ $message }}</strong>
                                    </span>
                        <br>

                        @enderror
                        <button type="submit" class="btn btn-primary mr-2">{{__('admin.save')}}</button>
                        <a class="btn btn-dark" href="{{route('Pages.index')}}"> {{__('admin.cancel')}}</a>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <!-- content-wrapper ends -->
@endsection
@section('script')
  <script>

    const wrapper = document.querySelector(".wrapper");
    const fileName = document.querySelector(".file-name");
    const defaultBtn = document.querySelector("#default-btn");
    const customBtn = document.querySelector("#custom-btn");
    const cancelBtn = document.querySelector("#cancel-btn i");
    const img = document.querySelector("#upload_image");
    let regExp = /[0-9a-zA-Z\^\&\'\@\{\}\[\]\,\$\=\!\-\#\(\)\.\%\+\~\_ ]+$/;
    function defaultBtnActive(){
        defaultBtn.click();
    }
    defaultBtn.addEventListener("change", function(){
        const file = this.files[0];
        if(file){
            const reader = new FileReader();
            reader.onload = function(){
                const result = reader.result;
                console.log(result)
                img.src = result;
                wrapper.classList.add("active");
            }
            cancelBtn.addEventListener("click", function(){
                img.src = "";
                wrapper.classList.remove("active");
            })
            reader.readAsDataURL(file);
        }
        if(this.value){
            let valueStore = this.value.match(regExp);
            fileName.textContent = valueStore;
        }
    });
  </script>
    @stop
