@extends('admin.layouts.master')
@section('content')


<div class="col-lg-12 grid-margin stretch-card" >
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">{{__('admin.pages')}}</h4>
            @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">
                {{Session::get('error')}}
            </div>
            @endif
            <span> {{__('admin.show')}}</span>
            <select name="perPage" id="perPage"  onchange="changepaginate()">

                <option value="5" @if($per_page==5) selected @endif>5</option>
                <option value="10" @if($per_page==10) selected @endif>10</option>
                <option value="25" @if($per_page==25) selected @endif>25</option>
                <option value="50" @if($per_page==50) selected @endif>50</option>
            </select>
            <span>{{__('admin.entries')}}</span>
            <br><br>
            <form class="form-inline" method="GET" id="filter_form">
                <div class="form-group mb-2">
                    <label for="filter" class="col-sm-2 col-form-label" >Filter</label>
                    <input type="text" class="form-control" id="filter" name="filter" placeholder="Product name..."  >
                </div>
{{--                <button type="submit" class="btn btn-default mb-2">Filter</button>--}}
            </form>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>@sortablelink( 'id',__('admin.id'))</th>
                        <th> @sortablelink('name_en',__('admin.name_en')) </th>
                        <th> @sortablelink('name_ar',__('admin.name_ar')) </th>
                        <th> @sortablelink('created_at',__('admin.created_time')) </th>
                        <th> @sortablelink('updated_at',__('admin.updated_time')) </th>
                        <th> {{__('admin.action')}} </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($pages as $page)

                        <tr>
                        <td class="py-1">
                          {{$page['id']}}
                        </td>
                        <td>{{$page['name_en']}} </td>
                        <td>
                            {{$page['name_ar']}}
                        </td>
                        <td> {{$page['created_at']}}  </td>
                        <td> {{$page['updated_at']}}  </td>
                            <td>
                                <a href="" onclick="event.preventDefault(); deleteObject( '{{ route('Pages.destroy',$page['id']) }}' ) " style="list-style-type: none"><li class="mdi mdi-delete" style="font-size: 20px;"></li></a>
                                <a href="{{ route('Pages.edit',$page['id']) }}" style="list-style-type: none"><li class="mdi mdi-table-edit" style="font-size: 20px;"></li></a>
                            </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>

                <br><br>
                <div class="d-flex justify-content-center">
                    {{ $pages->appends(compact('per_page'))->links() }}

                </div>
                <p>
                    {{__('admin.displaying')}} {{$pages->count()}} {{__('admin.of')}} {{ $pages->total() }} {{__('admin.pages')}}.
                </p>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        const input = document.getElementById("filter");

        input.addEventListener('input', submitform);



        function deleteObject(url) {


            Swal.fire({
                title: ' <div style="color: black;" >{!! trans('admin.are_you_sure') !!}</div>',
                icon:"question",
                showCancelButton: true,
                confirmButtonText: '{!! trans('admin.confirm') !!}',
            }).then(function (result) {
                if (result.value != null) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {_method: 'DELETE', _token: '{{ csrf_token() }}'},
                        success: function (message) {
                            // location.reload();
                            if (message['error'])
                            {swal.fire(' <div style="color: black;" >{!! trans('admin.fail') !!}</div>',
                                    message['error'],
                                    "error");}
                            else
                            {
                                swal.fire(' <div style="color: black;" >{!! trans('admin.success') !!}</div>',
                                    message['success'],
                                    "success").then(function (result) {
                                    if (result.value != null) {
                                        location.reload();

                                    }
                                });
                            }
                        },
                        fail: function (e) {
                            swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
                        }
                    });
                }
            })
        }
        function changepaginate() {
            var paginate = document.getElementById("perPage").value;
            window.location = "{!! $pages->url(1) !!}&per_page=" + paginate;

        }
        function submitform() {
            console.log('ola')

            value = $('#filter').val();
            per_page = $('#perPage').val();
            $.ajax({
                type: 'get',
                url: '{{route('search')}}',
                data: {
                    'filter': value,
                    'per_page': per_page
                },
                success: function (data) {
                    $('tbody').html(data);
                }
            });
        }
{{--            document.getElementById("filter").focus();--}}


    </script>
@stop
