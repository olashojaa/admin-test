@extends('admin.layouts.master')
@section('content')


    <div class="col-lg-12 grid-margin stretch-card" >
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{__('admin.pages')}}</h4>
                @if(Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{Session::get('error')}}
                    </div>
                @endif

                <div class="table-responsive">
                    <table id="products-table" class="table table-bordered table-hover" class="display" style="width:100%">
                        <thead>
                            <th>@sortablelink( 'id',__('admin.id'))</th>
                            <th> @sortablelink('name_en',__('admin.name_en')) </th>
                            <th> @sortablelink('name_ar',__('admin.name_ar')) </th>
                            <th> @sortablelink('created_at',__('admin.created_time')) </th>
                            <th> @sortablelink('updated_at',__('admin.updated_time')) </th>
                            <th> {{__('admin.action')}} </th>

                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                    <br><br>
                    <div class="d-flex justify-content-center">
{{--                        {{ $pages->appends(compact('per_page'))->links() }}--}}

                    </div>
                    <p>
{{--                        {{__('admin.displaying')}} {{$pages->count()}} {{__('admin.of')}} {{ $pages->total() }} {{__('admin.pages')}}.--}}
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('#products-table').DataTable({
                "serverSide": true,
                "ajax": {
                    url: "{{ action('App\Http\Controllers\admin\PageController@productsDataSource') }}",
                    method: "get"
                },
                "columnDefs" : [{
                    'targets': [4],
                    'orderable': false
                }],
            });
        });
        const input = document.getElementById("filter");

        input.addEventListener('input', submitform);



        function deleteObject(url) {


            Swal.fire({
                title: ' <div style="color: black;" >{!! trans('admin.are_you_sure') !!}</div>',
                icon:"question",
                showCancelButton: true,
                confirmButtonText: '{!! trans('admin.confirm') !!}',
            }).then(function (result) {
                if (result.value != null) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {_method: 'DELETE', _token: '{{ csrf_token() }}'},
                        success: function (message) {
                            // location.reload();
                            if (message['error'])
                            {swal.fire(' <div style="color: black;" >{!! trans('admin.fail') !!}</div>',
                                message['error'],
                                "error");}
                            else
                            {
                                swal.fire(' <div style="color: black;" >{!! trans('admin.success') !!}</div>',
                                    message['success'],
                                    "success").then(function (result) {
                                    if (result.value != null) {
                                        location.reload();

                                    }
                                });
                            }
                        },
                        fail: function (e) {
                            swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
                        }
                    });
                }
            })
        }
        {{--function changepaginate() {--}}
        {{--    var paginate = document.getElementById("perPage").value;--}}
        {{--    window.location = "{!! $pages->url(1) !!}&per_page=" + paginate;--}}

        {{--}--}}
        function submitform() {
            console.log('ola')

            value = $('#filter').val();
            per_page = $('#perPage').val();
            $.ajax({
                type: 'get',
                url: '{{route('search')}}',
                data: {
                    'filter': value,
                    'per_page': per_page
                },
                success: function (data) {
                    $('tbody').html(data);
                }
            });
        }
        {{--            document.getElementById("filter").focus();--}}


    </script>
@stop
