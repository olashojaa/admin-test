<!-- container-scroller -->
<!-- plugins:js -->
<script src={{url('admin/vendors/js/vendor.bundle.base.js')}}></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<script src={{url('admin/vendors/chart.js/Chart.min.js')}}></script>
<script src={{url('admin/vendors/progressbar.js/progressbar.min.js')}}></script>
<script src={{url('admin/vendors/jvectormap/jquery-jvectormap.min.js')}}></script>
<script src={{url('admin/vendors/jvectormap/jquery-jvectormap-world-mill-en.js')}}></script>
<script src={{url('admin/vendors/owl-carousel-2/owl.carousel.min.js')}}></script>
<!-- End plugin js for this page -->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<!-- inject:js -->
<script src={{url('admin/js/off-canvas.js")')}}></script>
<script src={{url('admin/js/hoverable-collapse.js')}}></script>
<script src={{url('admin/js/misc.js')}}></script>
<script src={{url('admin/js/settings.js')}}></script>
<script src={{url('admin/js/todolist.js')}}></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>


<!-- endinject -->
<!-- Custom js for this page -->
<script src={{url('admin/js/dashboard.js')}}></script>
<!-- End custom js for this page -->

