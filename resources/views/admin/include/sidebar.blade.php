
    <!-- partial:partials/_sidebar.html -->
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <div class="sidebar-brand-wrapper d-none d-lg-flex  fixed-top" style="padding-top: 15px;
    padding-left: 25px;">
           <p class="text-white" style="font-size: 25px;">{{__('admin.project_name')}}</p>
        </div>
        <ul class="nav">
            <li class="nav-item profile">
                <div class="profile-desc">
                    <div class="profile-pic">
                        <div class="count-indicator">
                            <img class="img-xs rounded-circle " src={{url( Auth::user()->photo )}} alt="">
                            <span class="count bg-success"></span>
                        </div>
                        <div class="profile-name">
                            <h5 class="mb-0 font-weight-normal"> {{ Auth::user()->name }}</h5>
                        </div>
                    </div>

                </div>
            </li>
            <li class="nav-item menu-items">
                <a class="nav-link" href="{{route('home')}}">
              <span class="menu-icon">
                <i class="mdi mdi-speedometer"></i>
              </span>
                    <span class="menu-title">{{__('admin.website')}}</span>
                </a>
            </li>


            <li class="nav-item menu-items">
                <a class="nav-link" href="">
              <span class="menu-icon">
                <i class="mdi mdi-speedometer"></i>
              </span>
                    <span class="menu-title">{{__('admin.dashboard')}}</span>
                </a>
            </li>


            <li class="nav-item menu-items">
                <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <span class="menu-icon">
                <i class="mdi mdi-laptop"></i>
              </span>
                    <span class="menu-title">{{__('admin.pages')}}</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="collapse" id="ui-basic">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item"> <a class="nav-link" href="{{route('Pages.index')}}">{{__('admin.list')}}</a></li>
                        <li class="nav-item"> <a class="nav-link" href="{{route('Pages.create')}}">{{__('admin.add')}} {{__('admin.page')}}</a></li>
                    </ul>
                </div>
            </li>


        </ul>
    </nav>
