
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Control Panel</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href={{url("admin/vendors/mdi/css/materialdesignicons.min.css")}}>
    <link rel="stylesheet" href={{url("admin/vendors/css/vendor.bundle.base.css")}}>
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href={{url("admin/vendors/jvectormap/jquery-jvectormap.css")}}>
    <link rel="stylesheet" href={{url("admin/vendors/flag-icon-css/css/flag-icon.min.css")}}>
    <link rel="stylesheet" href={{url("admin/vendors/owl-carousel-2/owl.carousel.min.css")}}>
    <link rel="stylesheet" href={{url("admin/vendors/owl-carousel-2/owl.theme.default.min.css")}}>
    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    @if(LaravelLocalization::getCurrentLocale()=="ar")
        <link rel="stylesheet" href={{url("admin/css/rtl.css")}}>
    @else
    <link rel="stylesheet" href={{url("admin/css/style.css")}}>
    @endif
    <!-- End layout styles -->
    <link rel="shortcut icon" href={{url("admin/images/favicon.png" )}}/>
</head>
