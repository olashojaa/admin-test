<?php

use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

    Route::get('/Dashboard', function () {
        return view('admin.index');
    })->name('Dashboard');
Route::resource('/Pages', App\Http\Controllers\admin\PageController::class);
Route::get('/search','App\Http\Controllers\admin\PageController@search')->name('search');
Route::get('/Pages_index2','App\Http\Controllers\admin\PageController@indexDatatable')->name('search');
Route::get('datatable_show', [
    'uses' => 'App\Http\Controllers\admin\PageController@productsDataSource'
]);
