<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify'=>true]);

Route::get('/', function () {
    return view('frontend.index');
})->middleware(['auth','verified'])->name('home');

Route::get('redirect/{service}','SocialController@redirect');
Route::get('callback/{service}','SocialController@callback');
Route::get('send_mail', 'HomeController@send_emails');
Route::post('admin/Pages/store', 'HomeController@send_emails');

