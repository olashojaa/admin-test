<?php

namespace App\Models;

use http\Env\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;


class Page extends Model
{
    use Sortable;
    use HasFactory;
    protected $table="pages";
    protected $fillable=['name_en','name_ar','icon','created_at','updated_at','photo'];
    public $sortable =['name_en','name_ar','id','created_at','updated_at'];
//    protected $hidden=['name_en'];

}
