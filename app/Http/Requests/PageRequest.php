<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Request;

class PageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(request()->isMethod('put')) // could be patch as well
        {
            return [
                'name_ar'=>'required|min:3|max:100|unique:pages,name_ar,'.$this->segment(4),
                'name_en'=>'required|min:3|max:100|unique:pages,name_en,'.$this->segment(4),
                'photo'=>'',

            ];
        }
        else{
            return [
                'name_ar'=>'required|min:3|max:100|unique:pages,name_ar,'.$this->segment(4),
                'name_en'=>'required|min:3|max:100|unique:pages,name_en,'.$this->segment(4),
                'photo'=>'required',

            ];
        }

    }

    public function messages()
    {
        return [
            'name_ar.required'=>__('admin.error_name_ar'),
            'name_en.required'=>__('admin.error_name_en'),
            'name_en.min'=>__('admin.error_name_en_min'),
            'name_en.max'=>__('admin.error_name_en_max'),
            'name_en.unique'=>__('admin.error_name_en_unique'),
            'name_ar.unique'=>__('admin.error_name_ar_unique'),
            'name_ar.min'=>__('admin.error_name_ar_min'),
            'name_ar.max'=>__('admin.error_name_ar_max'),
            'photo.required'=>__('admin.error_photo'),

        ];
    }


}
