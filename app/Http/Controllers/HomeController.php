<?php

namespace App\Http\Controllers;

use App\Mail\NotifyEmail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('frontend.home');
    }

    public function date()
    {
         return Carbon::now();
    }

    public function send_emails()
    {
        $emails=User::Pluck('email')->toArray();
        $data=['title'=>'php','body'=>'progrsmming'];
        foreach ($emails as $email)
        {
            Mail::to($email)->send(new NotifyEmail($data));
        }
    }
}
