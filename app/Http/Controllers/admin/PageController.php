<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PageRequest;
use App\Models\Page;
use App\Traits\PageTrait;
//use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    use PageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->per_page ?? 10;
//        $filter = $request->query('filter');
//        if (!empty($filter)) {
//            $pages = Page::sortable()
//                ->where('name_en', 'like', '%'.$filter.'%')
//                ->orwhere('name_ar', 'like', '%'.$filter.'%')
//                ->paginate($per_page);
//        } else {
            $pages = Page::sortable()
                ->paginate($per_page);
//        }


        return view('admin.pages.index',compact('pages','per_page') );

    }
    public function indexDatatable()
    {
        return view('admin.pages.index_datatable');
    }
    public function productsDataSource(Request $request) {

        $search = $request->query('search', array('value' => '', 'regex' => false));
        $draw = $request->query('draw', 0);
        $start = $request->query('start', 0);
        $length = $request->query('length', 25);
        $order = $request->query('order', array(1, 'asc'));
var_dump($request->query());
        $filter = $search['value'];

        $sortColumns = array(
            0 => 'pages.id',
            1 => 'pages.name_en',
            2 => 'pages.name_ar',
            3 => 'pages.created_at',
            4 => 'pages.updated_at',
        );

        $query = Page::all();

        if (!empty($filter)) {
            $query->where('name_en', 'like', '%'.$filter.'%')
                ->orwhere('name_ar', 'like', '%'.$filter.'%');
        }

        $recordsTotal = $query->count();

        $sortColumnName = $sortColumns[$order[0]['column']];
var_dump($sortColumnName);exit;
        $query->sortBy($sortColumnName)
            ->take($length)
            ->skip($start);

        $json = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsTotal,
            'data' => [],
        );

        $products = $query;
        foreach ($products as $product) {

            $json['data'][] = [
                $product->id,
                $product->name_en,
                $product->name_ar,
                $product->created_at,
                $product->updated_at,
                 "<a href=\"\" onclick=\"event.preventDefault(); deleteObject( '{{ route('Pages.destroy',$product->id) }}' ) \" style=\"list-style-type: none\"><li class=\"mdi mdi-delete\" style=\"font-size: 20px;\"></li></a>
                                <a href=\"{{ route('Pages.edit',$product->id]) }}\" style=\"list-style-type: none\"><li class=\"mdi mdi-table-edit\" style=\"font-size: 20px;\"></li></a>"
//                view('products.actions', ['product' => $product])->render(),
            ];
        }

        return $json;
    }
public function search(Request $request)
{
    $filter = $request->filter;
    $per_page = $request->per_page;
        $pages = Page::sortable()
            ->where('name_en', 'like', '%'.$filter.'%')
            ->orwhere('name_ar', 'like', '%'.$filter.'%')
            ->paginate($per_page);
        if($pages)
        {
            $res='';
            foreach ($pages as $page)
            {
               $res.=' <tr>
                        <td class="py-1">'.
                          $page['id'].
                        '</td>
                        <td>'.$page['name_en'].' </td>
                        <td>'.
                            $page['name_ar'].'
                        </td>
                        <td>' .$page['created_at'].'  </td>
                        <td> '.$page['updated_at'].'  </td>
                            <td>'.'
                                <a href="" onclick="event.preventDefault(); deleteObject( \''. route('Pages.destroy',$page['id']) .' ) " style="list-style-type: none"><li class="mdi mdi-delete" style="font-size: 20px;"></li></a>
                                <a href="'. route('Pages.edit',$page['id']) .'" style="list-style-type: none"><li class="mdi mdi-table-edit" style="font-size: 20px;"></li></a>
                            </td>
                    </tr>';
            }
        }
        return $res;
}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return View('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {
//validate request
    $validator=Validator::make($request->all(),$request->rules(),$request->messages());
    if($validator->fails())
        return redirect()->back()->withErrors($validator)->withInput($request->all());

    //save image in folder page
        $path='admin/images/pages/';
        $file_name=$this->saveImage($request->photo,$path);

        Page::insert(
        ['name_en'=>$request->name_en,
        'name_ar'=>$request->name_ar,
            'photo'=>$path.$file_name,
        ],
    );
        return redirect()->back()->with(['success'=>__('admin.success_msg')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return 'show';

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page=Page::find($id);
        if(!$page)
            return redirect()->back()->with(['error'=>__('admin.id_not_found')]);

        return View('admin.pages.edit',compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PageRequest $request, $id)
    {
        $validator=Validator::make($request->all(),$request->rules(),$request->messages());
        if($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        if($request->hasFile('photo')) {
            $path = 'admin/images/pages/';
            $file_name = $this->saveImage($request->photo, $path);
            Page::where('id', $id)->
            update(
                ['name_en' => $request->name_en,
                    'name_ar' => $request->name_ar,
                    'photo' => $path . $file_name],
            );
        }
        else{
            Page::where('id', $id)->
            update(
                ['name_en' => $request->name_en,
                    'name_ar' => $request->name_ar],
            );
        }
        return redirect()->back()->with(['success'=>__('admin.success_msg')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $page=Page::find($id);
        if(!$page)
            return ['error'=>__('admin.id_not_found')];
        else {
          Page::destroy($id);
            return ['success' => __('admin.success_msg')];
        }

    }

}
