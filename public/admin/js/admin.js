function deleteObject(url) {
    // Swal.fire({
    //     title: 'clients.are_you_sure',
    //     type: 'warning',
    //     showCancelButton: true,
    //     confirmButtonColor: '#3085d6',
    //     cancelButtonColor: '#d33',
    //     confirmButtonText: 'clients.delete',
    //     cancelButtonText: 'clients.cancel',
    //     confirmButtonClass: 'btn btn-primary',
    //     cancelButtonClass: 'btn btn-danger ml-1',
    //     buttonsStyling: false,
    // }).then(function (result) {
    //     if (result.value != null) {
    //         $.ajaxSetup({
    //             headers: {
    //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //             }
    //         });
    //
    //         $.ajax({
    //             url: url,
    //             type: 'POST',
    //             data: {_method: 'DELETE', _token: '{{ csrf_token() }}'},
    //             success: function (message) {
    //                 location.reload();
    //             },
    //             fail: function (e) {
    //                 swal("{!! trans('clients.fail') !!}", e.responseJSON.message, "error")
    //             }
    //         });
    //     }
    // });

    Swal.fire({
        title: ' <div style="color: black;" >Are You sure ?</div>',
        icon:"question",
        showCancelButton: true,
        confirmButtonText: `confirm`,
        }).then(function (result) {
        if (result.value != null) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: url,
                type: 'DELETE',
                data: {_method: 'DELETE', _token: '{{ csrf_token() }}'},
                success: function (message) {
                    location.reload();
                },
                fail: function (e) {
                    swal("{!! trans('clients.fail') !!}", e.responseJSON.message, "error")
                }
            });
        }
    })
}
